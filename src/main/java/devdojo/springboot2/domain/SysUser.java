package devdojo.springboot2.domain;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Entity
public class SysUser implements UserDetails {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;
    private String name;
    @Column(unique = true)
    private String username;
    private String password;
    private String token;
    private String authorities; // As authorities estarao separadas por virgulas Ex: ROLE_ADMIN,ROLE_USER

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
//        return Arrays.stream(authorities.split(",")).map(new Function<String, SimpleGrantedAuthority>() {
//            @Override
//            public SimpleGrantedAuthority apply(String role) {
//                System.out.println(role);
//                return new SimpleGrantedAuthority(role);
//            }
//        }).collect(Collectors.toList());

        List<SimpleGrantedAuthority> authorityList=new ArrayList<>();
        Iterator<String> iterator= Arrays.stream(authorities.split(",")).iterator();

        while (iterator.hasNext()){
            authorityList.add(new SimpleGrantedAuthority(iterator.next()));
        }
        return authorityList;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true; // A conta nao esta expirada
    }

    @Override
    public boolean isAccountNonLocked() {
        return true; // A conta nao esta bloqueada
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true; // A credencial nao esta expirada
    }

    @Override
    public boolean isEnabled() {
        return true; // A conta sempre estara abilitada
    }

    public String getUserName() {
        return username;
    }

    public void setUserName(String userName) {
        this.username = userName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setAuthorities(String authorities) {
        this.authorities = authorities;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
