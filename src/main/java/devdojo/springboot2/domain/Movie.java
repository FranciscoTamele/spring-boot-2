package devdojo.springboot2.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
public class Movie {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;
    @NotEmpty(message = "O campo nao esta estar vazio ou nulo.")
    @NotNull(message = "O campo nao esta estar vazio ou nulo.")
    private String title;
    @NotEmpty(message = "O campo nao esta estar vazio ou nulo.")
    @NotNull(message = "O campo nao esta estar vazio ou nulo.")
    private String genero;
    /*
    Essa anotacao e usada para o caso de a variavel da classe nao ter o mesmo nome com a variavel do JSON enviado pelo cliente.
    {
        "id":1,
        "tittle":"007",
        "year":2007
    }
    */
    @JsonProperty("year")
    private Integer release_year;
    @ManyToOne
    private Actor actor;

    public Movie() {
    }

    public Movie(Integer id, String title, Integer release_year) {
        this.id = id;
        this.title = title;
        this.release_year = release_year;
    }

    public String getTitle() {
        return title;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setTitle(String tittle) {
        this.title = tittle;
    }

    public Integer getRelease_year() {
        return release_year;
    }

    public void setRelease_year(Integer release_year) {
        this.release_year = release_year;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public Actor getActor() {
        return actor;
    }

    public void setActor(Actor actor) {
        this.actor = actor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Movie movie = (Movie) o;
        return Objects.equals(id, movie.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Movie{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", genero='" + genero + '\'' +
                ", release_year=" + release_year +
                ", actor=" + actor +
                '}';
    }
}
