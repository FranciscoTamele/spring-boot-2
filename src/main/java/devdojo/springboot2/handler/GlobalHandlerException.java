package devdojo.springboot2.handler;

import devdojo.springboot2.exception.MyException;
import devdojo.springboot2.exception.ResponseException;
import devdojo.springboot2.util.AuxFuncoes;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.LocalDateTime;

@ControllerAdvice
public class GlobalHandlerException {

    /**@_Costumize_all_exception_we_want
     * Ao colocar essa anotacao nesta classe informamos aos Controllers que ela sera responsavel em tratar todas excecoes
     * que forem colocadas os seus ExceptionHandler nesta classe.
     * */

    @Autowired
    private AuxFuncoes auxFuncoes;

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<Object> handleConstraintViolationException(ConstraintViolationException exception){
        return new ResponseEntity(createResponse(HttpStatus.BAD_REQUEST,
                "Restricao ",
                auxFuncoes.detectConstrainsViolation(exception),
                null),HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MyException.class)
    public ResponseEntity<Object> handlerMyException(MyException exception){
        return new ResponseEntity(createResponse(exception.getHttpStatus(),
                exception.getTittle(),
                exception.getDetails(),
                null),exception.getHttpStatus());
    }

    private ResponseException createResponse(HttpStatus httpStatus, String tittle, String details, String path){
        ResponseException exception=new ResponseException();
        exception.setTimestamp(LocalDateTime.now().toString());
        exception.setStatus(httpStatus.value());
        exception.setTittle(tittle);
        exception.setDetails(details);
        exception.setPath(path);
        return exception;
    }



}
