package devdojo.springboot2.handler;

import devdojo.springboot2.exception.ErrorDetails;
import devdojo.springboot2.exception.MyException;
import devdojo.springboot2.exception.ResponseException;
import devdojo.springboot2.exception.ValidationException;
import devdojo.springboot2.util.AuxFuncoes;
import devdojo.springboot2.util.FieldDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;

@ControllerAdvice
public class RestHandlerException extends ResponseEntityExceptionHandler {

    @Autowired
    private AuxFuncoes auxFuncoes;

    @Override
    protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

        ResponseException responseException=new ResponseException();
        responseException.setTimestamp(LocalDateTime.now().toString());
        responseException.setStatus(status.value());
        responseException.setTittle("Requisicao errada");
        responseException.setDetails("O endpoint nao existe");
        responseException.setPath(ex.getRequestURL());
        return new ResponseEntity<>(responseException,status);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {


        ValidationException validationException=new ValidationException();

        validationException.setTimestamp(LocalDateTime.now().toString());
        validationException.setTittle("Erro no preenchimento do formulario.");
        validationException.setDetails("Alguns campos nao foram preenchidos devidamento.");
        validationException.setStatus(status.value());
        validationException.setFields(auxFuncoes.getFieldsError(ex.getFieldErrors()));


        return new ResponseEntity<>(validationException,status);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

        ErrorDetails errorDetails=new ErrorDetails();
        errorDetails.setTimestamp(LocalDateTime.now().toString());
        errorDetails.setStatus(status.value());
        errorDetails.setTittle("Formato Json Errado.");
        errorDetails.setDetails("Certifique-se de que o arquivo json esta bem estrututrado.");

        return new ResponseEntity<>(errorDetails,status);
    }
}
