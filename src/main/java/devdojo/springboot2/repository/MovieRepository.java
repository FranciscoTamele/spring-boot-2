package devdojo.springboot2.repository;

import devdojo.springboot2.domain.Movie;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MovieRepository extends JpaRepository<Movie,Integer> {

    /**
     * O Spring vai mapear automaticamente, ira procurar o atributo genero na classe Movie
     * */
    List<Movie> findByGenero(String genero);

}
