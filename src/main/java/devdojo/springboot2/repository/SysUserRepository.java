package devdojo.springboot2.repository;

import devdojo.springboot2.domain.SysUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SysUserRepository extends JpaRepository<SysUser,Integer> {

    /**
     * O metodo acima findByUsername logo assume que na classe SysUser tem
     * um atributo com nome "username" caso nao tenha retornara um erro.
     * */
    SysUser findByUsername(String username);

}
