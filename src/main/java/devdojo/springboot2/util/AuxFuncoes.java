package devdojo.springboot2.util;

import devdojo.springboot2.domain.SysUser;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.stereotype.Component;
import org.springframework.validation.FieldError;

import java.util.ArrayList;
import java.util.Base64;
import java.util.Iterator;
import java.util.List;

@Component
public class AuxFuncoes {

    public List<FieldDetails> getFieldsError(List<FieldError> list){
        List<FieldDetails> listField=new ArrayList<>();
        Iterator<FieldError> it=list.iterator();
        while(it.hasNext()){
            FieldError fieldError=it.next();
            FieldDetails fieldDetails=new FieldDetails(fieldError.getField(),fieldError.getDefaultMessage());
            if(!listField.contains(fieldDetails)){
                listField.add(fieldDetails);
            }
        }
        return listField;
    }

    public String createToken(SysUser sysUser){
        StringBuilder builder=new StringBuilder();
        builder.append(sysUser.getUserName());
        builder.append(":");
        builder.append(sysUser.getPassword());
        return new String(Base64.getEncoder().encode(builder.toString().getBytes()));
    }

    public String detectConstrainsViolation(ConstraintViolationException violationException){
        System.out.println(violationException.getSQLException().getMessage());
        if(violationException.getSQLException().getMessage().equals("Cannot delete or update a parent row: a foreign key constraint fails (`sbessentials2`.`movie`, CONSTRAINT `FKqm7uqiqr4wipni1thpeemawuv` FOREIGN KEY (`actor_id`) REFERENCES `actor` (`id`))")){
            return "Nao e possivel deletar o actor, pode estar associado algum filme.";
        }else if(violationException.getSQLException().getMessage().equals("Cannot add or update a child row: a foreign key constraint fails (`sbessentials2`.`movie`, CONSTRAINT `FKqm7uqiqr4wipni1thpeemawuv` FOREIGN KEY (`actor_id`) REFERENCES `actor` (`id`))")) {
            return "O actor que pretende associar ao filme nao existe.";
        }else{
            return "Restricao, erro inesperado.";
        }
    }
}
