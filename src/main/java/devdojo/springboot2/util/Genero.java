package devdojo.springboot2.util;

import org.springframework.stereotype.Service;

public enum Genero {
    COMEDIA("Comedia"),
    ACCAO("Accao"),
    TERROR("Terror"),
    ROMANCE("Romance"),
    FICCAO("Ficcao");

    public String value;

    private Genero(String value){
        this.value=value;
    }

    public String getGenero(){
        return this.value;
    }
}
