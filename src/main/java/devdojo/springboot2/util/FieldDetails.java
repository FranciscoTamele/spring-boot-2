package devdojo.springboot2.util;

import java.util.Objects;

public class FieldDetails {

    private String field;
    private String fieldDescripption;

    public FieldDetails(String field, String fieldDescripption) {
        this.field = field;
        this.fieldDescripption = fieldDescripption;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getFieldDescripption() {
        return fieldDescripption;
    }

    public void setFieldDescripption(String fieldDescripption) {
        this.fieldDescripption = fieldDescripption;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FieldDetails that = (FieldDetails) o;
        return Objects.equals(field, that.field);
    }

    @Override
    public int hashCode() {
        return Objects.hash(field);
    }
}
