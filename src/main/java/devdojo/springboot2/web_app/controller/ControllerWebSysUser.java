package devdojo.springboot2.web_app.controller;

import devdojo.springboot2.domain.SysUser;
import devdojo.springboot2.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/web/sys-user")
//@PreAuthorize("hasRole('ADMIN')") Configurado na classe de seguranca
public class ControllerWebSysUser {

    @Autowired
    private SysUserService service;

    @GetMapping
    public ResponseEntity<Object> getUsers(){
        return new ResponseEntity<>(service.getUsers(), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findById(@PathVariable("id") int id){
        return new ResponseEntity<>(service.findById(id),HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Object> createUser(@RequestBody SysUser sysUser){
        return new ResponseEntity<>(service.create(sysUser),HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteUser(@PathVariable("id") int id){
        service.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<Object> updateUser(@RequestBody SysUser sysUser){
        return new ResponseEntity<>(service.update(sysUser),HttpStatus.CREATED);
    }


}
