package devdojo.springboot2.web_app.controller;

import devdojo.springboot2.domain.Actor;
import devdojo.springboot2.exception.My_ConstraintViolationException;
import devdojo.springboot2.service.ActorService;
import devdojo.springboot2.service.MoviesService;
import devdojo.springboot2.util.CONSUMER;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(path = "/web/actors")
public class ControllerWebActor {

    @Autowired
    MoviesService movieService;
    @Autowired
    private ActorService actorService;

    @GetMapping // Pagina lista actores
    public ModelAndView list_actor(@ModelAttribute("actor")Actor actor){
        ModelAndView model=new ModelAndView();
        model.addObject("actors",actorService.getAllActores());
        model.addObject("text");
        model.setViewName("list_actors");
        return model;
    }

    @PostMapping // Adiciona actor
    public String add(@ModelAttribute("actor")Actor actor){
        actorService.add(actor);
        return "redirect:/web/actors?sucesso_add";
    }

    @GetMapping("/form-a/{id}") // Formulario actualizacao actor
    public ModelAndView update_form(@PathVariable("id")int id,@ModelAttribute("actor") Actor actor){
        ModelAndView model=new ModelAndView();
        model.addObject("actor",actorService.getById(id));
        model.setViewName("update_actor");
        return model;
    }

    @PostMapping("form-a") // Gravar actualizacao actor
    public String update_save(@ModelAttribute("actor") Actor actor){
        actorService.update(actor);
        return "redirect:/web/actors?sucesso_upd";
    }

    @GetMapping("/delete/{id}") // Delete actor
    public String delete_(@PathVariable("id")int id) throws My_ConstraintViolationException {
        System.out.println("Entrou ************* Delete !!!");
        if(actorService.delete(id, CONSUMER.WEB_APP)){
            return "redirect:/web/actors?sucesso_rem";
        }else{
            return "redirect:/web/actors?sucesso_err";
        }
    }

}
