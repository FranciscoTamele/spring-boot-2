package devdojo.springboot2.web_app.controller;

import devdojo.springboot2.domain.Actor;
import devdojo.springboot2.domain.Movie;
import devdojo.springboot2.exception.My_ConstraintViolationException;
import devdojo.springboot2.service.ActorService;
import devdojo.springboot2.service.MoviesService;
import devdojo.springboot2.util.CONSUMER;
import devdojo.springboot2.util.Genero;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
//@RequestMapping("/web/thy/movies")
public class ControllerLearnThy {

    /**@_Redicionamento
     * forward = Prefixo usado para fazer um redirecionamento no lado do servidor. return "forward:/web/thy"
     * redirect = Prefixo usado para fazer um redirecionamento no lado do cliente. return "redirect:/web/thy"
     * */


}
