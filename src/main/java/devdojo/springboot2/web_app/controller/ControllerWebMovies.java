package devdojo.springboot2.web_app.controller;

import devdojo.springboot2.domain.Actor;
import devdojo.springboot2.domain.Movie;
import devdojo.springboot2.service.ActorService;
import devdojo.springboot2.service.MoviesService;
import devdojo.springboot2.util.CONSUMER;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(path="/web/movies")
public class ControllerWebMovies {
    /**
     * Os metodos desta classe praticamente estarao limpos e a logica de negocio estara na classe AnimeService no pacote service
     * */
    @Autowired
    MoviesService movieService;
    @Autowired
    private ActorService actorService;

    @GetMapping // Pagina lista filmes
    public ModelAndView getMovies(Pageable pageable){
        ModelAndView modelView=new ModelAndView();
        modelView.setViewName("home");
        modelView.addObject("movies", movieService.getMovies(pageable).getContent());
        return modelView;
    }

    @GetMapping(path="/form-m") // Formulario adicionar filme
    public ModelAndView add_form(@ModelAttribute("movie")Movie movie, @ModelAttribute("message") String message){
        ModelAndView model=new ModelAndView();
        model.addObject("actors",actorService.getAllActores());
        model.addObject("message","Filme adicionado com sucesso");
        model.setViewName("add_movie_form");
        return model;
    }

    @PostMapping(path="/from-m") // Adiciona filme
    public String add(@ModelAttribute("movie")Movie movie, @ModelAttribute("message") String message){
        if(movieService.add(movie,CONSUMER.WEB_APP)!=null){
            return "redirect:/web/movies?sucesso_add";
        }else{
            return "redirect:/web/movies?sucesso_err";
        }
    }

    @GetMapping("/form-a/{id}") // Formulario actualizacao filme
    public ModelAndView updateForm(@PathVariable("id") int id, @ModelAttribute("actors") Actor actor){
        ModelAndView modelView=new ModelAndView();
        modelView.addObject("movie",movieService.findById(id));
        modelView.addObject("actors",actorService.getAllActores());
        modelView.setViewName("update_form");
        return modelView;
    }

    @PostMapping("/form-a") // Grava actualizacao filme
    public String update(@ModelAttribute("movie") Movie movie, @ModelAttribute("actor")Actor actor){
        movieService.update(movie);
        return "redirect:/web/movies?sucesso_upd";
    }

    @GetMapping("/delete/{id}") // Deleta filme
    public String delete(@PathVariable("id") int id){
        movieService.remove(id);
        return "redirect:/web/movies?sucesso_rem";
    }


}
