package devdojo.springboot2.web_app.controller;

import devdojo.springboot2.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ControllerWebAuth {

    @Autowired
    SysUserService service;

    @GetMapping
    public String homee(){
        return "redirect:/web/movies";
    }

    @GetMapping("/web/login")
    public String index(){
        return "login";
    }

    /** @Informacoes_acerca_autenticacao_web
     * O Spring Security ja possui metodos Get e Post para endereco "/login" que o Get retorna o formulario e o
     * Post e chamado quando o usuario clica em "sign in" para autenticar as credenciais.
     *
     * .formLogin() // Nos possibilita fazer algumas configuracoes acerca da pagina de login
     * .loginProcessingUrl("/login") // Dizemos ao Spring que requisicoes que precisam de autenticacao, as credenciais devem ser validades nessa url que neste e Post "/login" do Spring
     * .loginPage("/web/login") // Definimos a nossa propria pagina de login
     *
     * */
}
