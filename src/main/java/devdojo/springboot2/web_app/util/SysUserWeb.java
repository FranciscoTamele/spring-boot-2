package devdojo.springboot2.web_app.util;

import org.springframework.stereotype.Component;

@Component
public class SysUserWeb {

    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "SysUserWeb{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
