package devdojo.springboot2.controller;

import devdojo.springboot2.domain.Actor;
import devdojo.springboot2.exception.My_ConstraintViolationException;
import devdojo.springboot2.service.ActorService;
import devdojo.springboot2.util.CONSUMER;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(path = "/actors")
public class ControllerActor {

    @Autowired
    private ActorService service;

    @GetMapping
    public ResponseEntity<Object> getActors(){
        return new ResponseEntity<>(service.getAllActores(), HttpStatus.OK);
    }

    @GetMapping(path="/{id}")
    public ResponseEntity<Object> getById(@PathVariable("id") int id){
        return new ResponseEntity<>(service.getById(id),HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<Object> update(@RequestBody Actor actor){
        return new ResponseEntity<>(service.update(actor),HttpStatus.OK);
    }

    @DeleteMapping(path="/{id}")
    public ResponseEntity<Object> delete(@PathVariable("id") int id) throws My_ConstraintViolationException {
        service.delete(id, CONSUMER.API);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
