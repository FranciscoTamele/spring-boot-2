package devdojo.springboot2.controller;

import devdojo.springboot2.domain.Movie;
import devdojo.springboot2.service.MoviesService;
import devdojo.springboot2.util.CONSUMER;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(path="/movies")
public class ControllerMovies {
    /**
     * Os metodos desta classe praticamente estarao limpos e a logica de negocio estara na classe AnimeService no pacote service
     * */
    @Autowired
    private MoviesService service;

    @GetMapping(produces= MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getMovies(Pageable pageable){
        /**@_Pageable
         * Mais informacoes do Pageable no MovieServices
         * */
        return new ResponseEntity<>(service.getMovies(pageable), HttpStatus.OK);
    }

    @GetMapping(path="/{id}")
    public ResponseEntity<Object> findById(@PathVariable("id") Integer id, @AuthenticationPrincipal UserDetails userDetails){
        return new ResponseEntity<>(service.findById(id),HttpStatus.OK);
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> add(@RequestBody @Valid Movie movie){
        return new ResponseEntity(service.add(movie, CONSUMER.API),HttpStatus.CREATED);
    }

    @DeleteMapping(path = "/{id}")
    @PreAuthorize("hasRole('ADMIN')") // Somente usuario com role ADMIN executarao esse metodo.
    public ResponseEntity<Object> delete(@PathVariable int id){
            service.remove(id);
            return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<Object> update(@RequestBody Movie movie){
        return new ResponseEntity<>(service.update(movie),HttpStatus.OK);
    }

    @GetMapping(path="/find")
    public ResponseEntity<List<Movie>> findByGenero(@RequestParam(name="genero") String genero){
        /** O @RequestParam e usado para pegar parametro passado desse jeito http://localhost:9010/movies/find?genero=comedia
         * O nome da variavel passado na url(No exemplo 'genero') deve ser o mesmo nome atribuito ao atributo name do @RequestParam.
         */
        return new ResponseEntity<>(service.getbyGenero(genero),HttpStatus.OK);
    }


}
