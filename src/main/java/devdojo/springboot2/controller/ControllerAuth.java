package devdojo.springboot2.controller;

import devdojo.springboot2.domain.SysUser;
import devdojo.springboot2.service.SysUserService;
import devdojo.springboot2.util.ModelUserAuth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ControllerAuth {

    @Autowired
    private SysUserService service;

    @PostMapping("/authenticate")
    public ResponseEntity<Object> authenticate(@RequestBody SysUser sysUser){
        ModelUserAuth sysUser1=service.authenticate(sysUser);
        return new ResponseEntity<>(sysUser1, HttpStatus.OK);
    }

}
