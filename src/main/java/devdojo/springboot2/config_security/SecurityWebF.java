package devdojo.springboot2.config_security;

import devdojo.springboot2.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true) // Utilizamos essa anotacao quando utilizamos restricao de roles nos metodos
public class SecurityWebF extends WebSecurityConfigurerAdapter {

    @Autowired
    private SysUserService sysUserService;

    /**@PreAuthorize
     *  Utilizado para proteger metodos de usuarios autenticados mas nao autorizados.
     *  Exemplo: Exemplo neste projecto tem as roles(ADMIN e USER):
     *  O usuario com funcao USER nao pode remover estudante apenas o usuario com role ADMIN.
     *
     *  NB: Necessario essa anotacao @EnableGlobalMethodSecurity(prePostEnabled = true) na classe responsavel pela autenticacao
     * */

    /**@AuthenticationPrincipal
     * Usuamos essa anotacao quando queremos pegar o usuario que esta autenticado(que esta fazendo a requisicao), colocamos como
     * parametro no respectivo metodo que queremos pegado.
     * Ex:
     * @GetMapping
     * public ResponseEntity<Object> add(@AuthenticationPrinciapl UserDetails userDetails){
     *
     * }
     * */

    /**@_Filtros_de_autenticacao_do_Spring
     * 1. BasicAuthenticactionFilter = autenticacao do tipo basic
     * Ex: No header da requisicao coloca-se o parametro
     * Authorization Basic ************* token *****************
     *
     * 2. UsernamePasswordAuthenticationFilter = Verifica se na requisicao tem usuario e password
     * 3. DefaultLoginPageGeneratingFilter
     * 4. DefaultLogoutPageGeneratingFilter
     * 5. FilterSecurityInterceptor = Verifica se usuario esta autorizado
     *
     * Na parte da seguranca temos dois processos: Autenticacao e Autorizacao
     * */


    @Override // Nesse metodo especificamos o que estamos protegendo com protocolo http
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers("/public/**").permitAll()
                .antMatchers("/authenticate").permitAll()
                .antMatchers("/sys-user").permitAll()
//                .antMatchers("/sys-user/**").hasRole("ADMIN")
                /**
                 * Todos usuarios que acessar a url /sys-user/ tem que ter role 'ADMIN'
                 * NB: No caso de termos varios antMatchers() ha uma regra a se seguir, a url mais restritiva deve vir primeiro
                 * Ex:
                 *      .antMatchers("/sys-user/**").hasRole("ADMIN")
                 *      .antMatchers("/**").hasRole("ADMIN")
                 * */
                .anyRequest()
                .authenticated()
                .and()
                .formLogin().loginProcessingUrl("/login")
                .loginPage("/web/login")
                .permitAll()
                .and()
                .logout().logoutSuccessUrl("/web/login?logout")
                .permitAll()
                .and()
                .httpBasic();


    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {

        auth.userDetailsService(sysUserService).passwordEncoder(new BCryptPasswordEncoder());

//      NB: O SPRING ACEITA VARIOS TIPOS DE AUTENTICACAO, EM MEMORIA E VIA BANCO DE DADOS
//        auth.inMemoryAuthentication()
//                .withUser("france")
//                .password("{bcrypt}"+new BCryptPasswordEncoder().encode("12345"))
//                .roles("ADMIN","USER")
//        .and()
//                .withUser("tamele")
//                .password("{bcrypt}"+new BCryptPasswordEncoder().encode("12345"))
//                .roles("USER");

//                    /**
//             * No campo password tem que se definir se o password  deve ser criptografado antes da verificacao se  as
//             * credenciais correspondem ou nao.
//             * Nos casos abaixo o password e o mesmo(12345) so que primeiro caso esta encryptado, portando para o usuario
//             * Francisco o Spring ao receber a password ira encrypta-la depois verificar se confere, encryptar usando o algoritmo
//             * BCryptPasswordEncoder.
//             * No segundo caso nao sera necessario.
//             * */
//            auth.inMemoryAuthentication()
//                    .withUser("Francisco").password("{bcrypt}$2a$10$54GG2k8UPfLSxZnVQgozLOe6b3jqKWdMNGXug49thz1rVerlwYYEi").roles("USER")
//                    .and().withUser("Tamele").password("{noop}12345").roles("ADMIN","USER");
    }

    /**@_Resources
     * Todos arquivos que estao na pasta resources quando activo o spring security tambem ficam protegidos,
     * portanto no caso de houver arquivo css, js, foto, etc deve ser desabilitada a seguranca para esses links.
     *
     * @_Caminhos_para_resources
     * Arquivos estao em "src/main/resources/static/hello.jpg" ficam acessiveis neste link http://localhost:9010/hello.jpg
     * Arquivos estao em "src/main/resources/static/public/hello.jpg" ficam acessiveis neste link http://localhost:9010/public/hello.jpg
     *
     * E recomendado criar dois directorios na pasta static "/public" e "/private" para no public colocar todos arquivos nao
     * necessitam de autenticacao e permitir o acesso na configuracao dos .antMatchers("/public/**").permitAll().
     *
     * @_Directorios
     * No directorio static geralmente ficam arquivos css, js, fotos, ou seja realmente estaticos.
     *
     * No directorio templates ficam as paginas html.
     *
     * @_NB: Por algum motivo ao adicionar a notacao @EnableWebMvc o handler dos recursos nao e ecnontrado.
     * */
}
