package devdojo.springboot2.exception;

import devdojo.springboot2.util.FieldDetails;

import java.util.List;

public class ValidationException extends ErrorDetails{

    List<FieldDetails> fields;

    public List<FieldDetails> getFields() {
        return fields;
    }

    public void setFields(List<FieldDetails> fields) {
        this.fields = fields;
    }
}
