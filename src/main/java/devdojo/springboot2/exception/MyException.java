package devdojo.springboot2.exception;

import org.springframework.http.HttpStatus;

public class MyException extends RuntimeException{

    private HttpStatus httpStatus;
    private String tittle;
    private String details;

    public MyException() {
        super();
    }

    public MyException(HttpStatus httpStatus, String tittle, String details) {
        super(details);
        this.httpStatus = httpStatus;
        this.tittle = tittle;
        this.details = details;
    }

    public String getTittle() {
        return tittle;
    }

    public void setTittle(String tittle) {
        this.tittle = tittle;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }
}
