package devdojo.springboot2.exception;

public class ResponseException extends ErrorDetails{

    private String path;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
