package devdojo.springboot2.service;

import devdojo.springboot2.domain.SysUser;
import devdojo.springboot2.exception.MyException;
import devdojo.springboot2.repository.SysUserRepository;
import devdojo.springboot2.util.AuxFuncoes;
import devdojo.springboot2.util.ModelUser;
import devdojo.springboot2.util.ModelUserAuth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.*;
import java.util.function.Supplier;

@Service
public class SysUserService implements UserDetailsService {

    @Autowired
    private SysUserRepository repository;

    @Autowired
    private AuxFuncoes auxFuncoes;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return Optional.ofNullable(repository.findByUsername(username)).orElseThrow(new Supplier<UsernameNotFoundException>() {
            @Override
            public UsernameNotFoundException get() {
                return new UsernameNotFoundException("credenciais invalidas.");
            }
        });
    }

    public List<ModelUser> getUsers() {
        List<SysUser> list = repository.findAll();
        List<ModelUser> list1 = new ArrayList<>();
        Iterator<SysUser> iterator = list.iterator();
        while (iterator.hasNext()) {
            list1.add(build(iterator.next()));
        }
        return list1;
    }

    public ModelUser findById(int id) {
        try {
            SysUser sysUser=repository.findById(id).get();
            return build(sysUser);
        } catch (NoSuchElementException ex) {
            throw new MyException(HttpStatus.BAD_REQUEST, "Requisicao invalida.","Usuario inexistente.");
        }
    }

    public ModelUser create(SysUser sysUser) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        sysUser.setPassword(encoder.encode(sysUser.getPassword()));
        return build(repository.save(sysUser));
    }

    public void delete(int id) {
        findById(id);
        repository.deleteById(id);
    }

    public ModelUserAuth authenticate(SysUser user){

            SysUser sysUser = repository.findByUsername(user.getUsername());
            if(sysUser==null){
                throw new MyException(HttpStatus.UNAUTHORIZED,"Credenciais invalidas.","O username ou senha estao incorrectos.");
            }else{
                if(new BCryptPasswordEncoder().matches(user.getPassword(),sysUser.getPassword())){
                    ModelUserAuth user1=new ModelUserAuth();
                    user1.setId(sysUser.getId());
                    user1.setName(sysUser.getName());
                    user1.setUsername(sysUser.getUserName());
                    user1.setNivel_acesso(sysUser.getAuthorities().toString());
                    user1.setToken(auxFuncoes.createToken(user));
                    return user1;
                }else{
                    throw new MyException(HttpStatus.UNAUTHORIZED,"Credenciais invalidas.","O username ou senha estao incorrectos.");
                }

            }

    }

    public ModelUser update(SysUser sysUser) {
        findById(sysUser.getId());
        return build(repository.save(sysUser));
    }

    private ModelUser build(SysUser sysUser){
        ModelUser modelUser=new ModelUser();
        modelUser.setId(sysUser.getId());
        modelUser.setName(sysUser.getName());
        modelUser.setNivel_acesso(sysUser.getAuthorities().toString());
        return modelUser;
    }
}
