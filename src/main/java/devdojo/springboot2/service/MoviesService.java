package devdojo.springboot2.service;

import devdojo.springboot2.domain.Movie;
import devdojo.springboot2.exception.MyException;
import devdojo.springboot2.repository.ActorRepository;
import devdojo.springboot2.repository.MovieRepository;
import devdojo.springboot2.util.CONSUMER;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@Service
public class MoviesService {

    @Autowired
    private MovieRepository repository;
    @Autowired
    private ActorRepository actorRepository;

    private static List<Movie> list=new ArrayList<>();

    public Page<Movie> getMovies(Pageable pageable){
        /**@_Pageable
         * A interface Pageable permite-nos limitar os dados retornado ao usuario e quando precisar de mais dados requisitara.
         * Os resultados sao organizaos em paginas e cada pagina tem uma certa quantidade de dados especificados pelo developer,
         * por padrao retorna a primeira pagina e cada pagina tem 20 elementos.
         *
         * Para espeficar outro numero de dados que devem conter as paginas, define-se na url.
         * @Ex:_http://localhost:9010/movies?size=5.
         * Cada pagina tera 5 elementos e nessa url retornara primeira pagina.
         *
         * Para requisitar outros dados(segunda pagina) segue-se:
         * @Ex:_http://localhost:9010/movies?size=5&page=2.
         * Retorna a pagina 2.
         *
         * Os dados retornados ao usuario pode ser ordenados por um dos atributos que compoem cada elemento(Classe), neste
         * exemplo iremos ordenar em orem crescente usando o atributo tittle.
         * @Ex:_http://localhost:9010/movies?size=5&page=2&sort=title,asc.
         * */
        return repository.findAll(pageable);
    }

    public Movie findById(Integer id){
        try{
            return repository.findById(id).get();
        }catch (NoSuchElementException ex){
            throw  new MyException(HttpStatus.BAD_REQUEST,"Requisicao errada","Movie does not exist.");
        }
    }

//    @Transactional
    public Movie add(Movie movie, CONSUMER consumer){
        /**@Transactional
         * Quando o metodo e anotado com essa anotacao o JPA nao faz commit das accoes antes de terminar o metodo, entao no caso
         * de alguma excecao sao canceladas as operacoes feitas anteriormente.
         *
         * NB: Ao colocar throws Exception neste metodo e no metodo do Controller que chama este, geralmente o rollback nao sera feito,
         * para tal deve-se seguir da seguinte forma:
         * @Transactional(rollbackFor=Exception.class)
         * */
        try {
            if (movie.getActor().getId() == null) { // No caso do actor nao existir, primeiro sera salvo posteriormente o filme
                movie.setActor(actorRepository.save(movie.getActor()));
            }else{

                movie.setActor(actorRepository.findById(movie.getActor().getId()).get());
            }
            return repository.save(movie);
        }catch (DataIntegrityViolationException ex){
            if(consumer==CONSUMER.API){
                throw ex;
            }
            return null;
        }
    }

    public void remove(Integer id){
        findById(id);
        repository.deleteById(id);
    }

    public Movie update(Movie movie){
        findById(movie.getId());
        return repository.save(movie);
    }

    public List<Movie> getbyGenero(String genero){
        return repository.findByGenero(genero);
    }

}
