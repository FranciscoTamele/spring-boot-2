package devdojo.springboot2.service;

import devdojo.springboot2.domain.Actor;
import devdojo.springboot2.exception.MyException;
import devdojo.springboot2.exception.My_ConstraintViolationException;
import devdojo.springboot2.repository.ActorRepository;
import devdojo.springboot2.util.CONSUMER;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@Service
public class ActorService {

    @Autowired
    private ActorRepository repository;

    public List<Actor> getAllActores(){
        return repository.findAll();
    }

    public Actor getById(int id){
        try {
            return repository.findById(id).get() ;
        }catch (NoSuchElementException ex){
            throw new MyException(HttpStatus.BAD_REQUEST,"Requisicao errada.","O actor nao existe.");
        }
    }

    public boolean delete(int id, CONSUMER consumer) {

            try{
                repository.delete(getById(id));
                return true;
            }catch (DataIntegrityViolationException ex){
                ex.printStackTrace();
                if(consumer==CONSUMER.API){
                    throw ex;
                }
                return false;
            }

    }

    public Actor update(Actor actor){
        getById(actor.getId());
        return repository.save(actor);
    }

    public Actor add(Actor actor){
        return repository.save(actor);
    }

}
