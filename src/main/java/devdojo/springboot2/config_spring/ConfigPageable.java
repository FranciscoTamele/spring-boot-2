package devdojo.springboot2.config_spring;


import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import java.util.List;

@Configuration
public class ConfigPageable implements WebMvcConfigurer {

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> resolvers) {
        /**@_Configurar_Pageable
         * Neste metodo configuramos que por padrao o spring deve retornar a primeira pagina e cada pagina deve ter 5 elemtnos.
         * */
        PageableHandlerMethodArgumentResolver pageHandler=new PageableHandlerMethodArgumentResolver();
        pageHandler.setFallbackPageable(PageRequest.of(0,20));
        resolvers.add(pageHandler);
    }
}
