package devdojo.springboot2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
public class Springboot2Application {

    public static void main(String[] args) {
        SpringApplication.run(Springboot2Application.class, args);
    }

    /**@Estrutura_do_projecto
     * Pacotes = Descricao
     * controller = basicamente so teremos os endpoints,
     * domain = Representa o que temos no banco de dados
     * service = Teremos as classes da logica de negocio
     * repository = Conexao banco de dados
     * */

    /**@HibernateValidator
     * Para fazer validacao dos campos pode se usar as notacoes do hibernate validator
     * */

    /**@Diferença_entre_PUT_e_PATCH
     * O PUT, é usado para alteração de um dado completo
     * O PATCH é usado para atualização parcial.
     * Em poucas palavras, os métodos HTTP PUT e PATCH são usados para indicar um requisição de alteração de dados.
     * Geralmente, ao usar-se o PUT, fica legível que a alteração do dado será com referência a entidade completa.
     * Exemplo: (/usuario/1234) :
     * Resultado: {'id': 1234, 'name': 'Joao', 'idade': 25, 'documento': '123.321.12-X'}
     * O PATCH é usado para atualização parcial, quando você não quer mandar o payload completo.
     * Exemplo: (/usuario/1234) :
     * Resultado: {'name': 'João'}
     * */

}
