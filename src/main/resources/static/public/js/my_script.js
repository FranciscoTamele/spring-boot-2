
function validarFormMovie(){
    var titulo=document.getElementById('lblTitulo');
    var data=document.getElementById('lblAno');
    var genero=document.getElementById('lblGenero');
    var actor=document.getElementById('lblActor');
    var mgs='';
    var flag=true;
    if(titulo.value==''){
        mgs='O campo titulo não pode estar vazio.\n';
        flag=false;
    }
    if(data.value==''){
        mgs=mgs+'O campo ano não pode estar vazio.\n';
        flag=false;
    }
    if(genero.value==''){
        mgs=mgs+'Selecione o genero do filme.\n';
        flag=false;
    }
    if(actor.value==''){
        mgs=mgs+'Selecione o actor do filme.\n';
        flag=false;
    }
    if(flag){
        return true;
    }else{
        alert(mgs);
        return false;
    }
}

function validarActor(){
    var nome=document.getElementById('lblNome');
    if(nome.value==''){
        alert('O campo nome não pode estar vazio.');
        return false;
    }
    return true;
}

